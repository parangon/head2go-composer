<?php
/**
 * Head2Go library (04.2024)
 **/
namespace Parangon\Head2go;

class Head2Go
{
    /** @var array */
    private array $defaultOptions = [
        'pre-title'     => "",
        'post-title'    => "",
        'tl-separator'  => " - ",
        'description'   => "",
        'keywords'      => [],
        'hrefLang'      => [],
        'bc-separator'  => '<span class="sep">&nbsp;&rsaquo;&nbsp;</span>',
        'bc-ucfirst'    => true,
        'ssl'           => false
    ];

    /** @var array custom name/route */
    private array $breadcrumbItems = [
        '/' => "Home"
    ];

    /** @var string */
    public string $domain;

    /** @var string */
    private string $url = "";

    /** @var array */
    private array $breadcrumbList = [];

    public function __construct(array $options = [], array $breadcrumbItems = [], string $domain = null)
    {
        $this->defaultOptions  = array_merge($this->defaultOptions, $options);
        $this->breadcrumbItems = array_merge($this->breadcrumbItems, $breadcrumbItems);

        $ssl = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443 || $this->defaultOptions['ssl'] === true;
        $this->domain = ($ssl ? 'https://' : 'http://') . ($domain ?? $_SERVER['HTTP_HOST']);
    }

    private function urlBuilder(string $string, bool $reset = false): string
    {
        $this->url = preg_replace("~/+~", "/", $reset ? $string : ($this->url . $string));
        $this->url = preg_replace("/:\//", "://", $this->url);
        return $this->url;
    }

    private function titleBuilder(string $title): string
    {
        $title  = !empty($this->defaultOptions['pre-title']) ? ($this->defaultOptions['pre-title'] . (!empty($title) ? $this->defaultOptions['tl-separator'] : "") . $title) : $title;
        $title .= !empty($this->defaultOptions['post-title']) ? ($this->defaultOptions['tl-separator'] . $this->defaultOptions['post-title']) : "";
        return $title;
    }

    public function renderTitle(string $title = ""): string
    {
        return sprintf('<title>%s</title>', $this->titleBuilder($title));
    }

    public function renderDescription(string $description = ""): string
    {
        $description = !empty($description) ? $description : $this->defaultOptions['description'];
        return sprintf('<meta name="description" content="%s" />', $description);
    }

    public function renderCanonical(string $url = ""): string
    {
        $url = empty($url) ? $this->domain . explode("?", $_SERVER['REQUEST_URI'])[0] : $url;
        return sprintf('<link rel="canonical" href="%s" />', $url);
    }

    public function renderHrefLang(array $alternates = []): string
    {
        $alternates = empty($alternates) ? $this->defaultOptions['hrefLang'] : $alternates;
        if(count($alternates) <= 1) return "";

        $xDefault = array_shift($alternates);
        $url  = $this->domain . explode("?", $_SERVER['REQUEST_URI'])[0];
        preg_match("/\/([a-zA-Z]{2}|[a-z-_A-Z]{5})(\/|$)/", $url, $matches);

        if(!isset($matches[1])) return "";
        else $url = str_replace("/$matches[1]", "/$xDefault", $url);

        $links = '<link rel="alternate" href="'.$url.'" hreflang="x-default" />';
        foreach ($alternates as $lang) {
            $href = str_replace("/$xDefault", "/$lang", $url);
            $links .= sprintf('<link rel="alternate" href="%s" hreflang="%s" />', $href, $lang);
        }

        return $links;
    }

    public function renderOpenGraph(array $openGraph = []): string
    {
        if(empty($openGraph)) return "";
        if(!isset($openGraph['type'])) $openGraph['type'] = 'website';
        if(!isset($openGraph['url'])) $openGraph['url'] = $this->domain . explode("?", $_SERVER['REQUEST_URI'])[0];

        $required = ['type', 'title', 'image', 'url', 'description'];
        foreach ($required as $index) {
            if(!array_key_exists($index, $openGraph)) return "<!-- missing og:$index -->";
        }

        $meta = "";
        foreach ($openGraph as $index => $value) {
            $prefix = $index === 'app_id' ? 'fb' : 'og';
            if($index === 'title') $value = $this->titleBuilder($value);
            $meta  .= sprintf('<meta property="%s:%s" content="%s" />', $prefix, $index, $value);
        }

        return $meta;
    }

    public function renderKeywords(array $keywords = []): string
    {
        $keywords = empty($keywords) ? $this->defaultOptions['keywords'] : $keywords;
        return empty($keywords) ? "" : sprintf('<meta name="keywords" content="%s" />', implode(", ", $keywords));
    }

    public function renderJsonLd(array $schema = []): string
    {
        return empty($schema) ? "" : sprintf('<script type="application/ld+json">%s</script>', json_encode($schema, JSON_UNESCAPED_SLASHES));
    }

    /**
     * @param array breadcrumbItems
     * Ex: ["name"] or ['name' => "name", 'route' => "/name"] or ['name' => ['fr' => "nom", 'en' => "name"], ...]
     * @return void
     */
    private function setBreadcrumbList(array $breadcrumbItems = []): void
    {
        $breadcrumbItems = array_merge($this->breadcrumbItems, $breadcrumbItems);

        $this->breadcrumbList = [];
        $lang = "";

        $this->urlBuilder($this->domain, true);
        $uri = explode("?", $_SERVER['REQUEST_URI'])[0];

        if(!empty($hrefLang = $this->defaultOptions['hrefLang'])) {
            foreach($hrefLang as $lang) {
                if (stripos($uri, "/$lang") === 0) {
                    $uri = ltrim($uri, "/");
                    break;
                }
            }
        }

        $routes = explode("/", $uri);
        foreach($routes as $i => $route) {
            if($i !== 0 && empty($route)) continue;

            $name  = str_replace(['-', '_'], " ", $route);
            $crumb = $breadcrumbItems["/$route"] ?? ['name' => $name, 'route' => "/$route"];

            if(!is_array($crumb)) $name = $crumb;
            else $name = $crumb['name'][$lang] ?? (is_array($crumb['name']) ? $name : $crumb['name']);

            $element = [];
            $element['name'] = $this->defaultOptions['bc-ucfirst'] ? ucfirst($name) : strtolower($name);
            $element['item'] = $this->urlBuilder($crumb['route'] ?? "/$route");
            $element['position'] = $i+1;

            $this->breadcrumbList[] = $element;
        }
    }

    public function renderJsonBreadcrumb(array $breadcrumbItems = []): string
    {
        $this->setBreadcrumbList($breadcrumbItems);
        if(count($this->breadcrumbList) <= 1) return "";

        $jsonLDBreadcrumb = [
            "@context" => "https://schema.org",
            "@type" => "BreadcrumbList",
            "itemListElement" => []
        ];

        foreach($this->breadcrumbList as $i => $crumb) {
            $jsonLDBreadcrumb['itemListElement'][] =  [
                "@type"     => "ListItem",
                "position"  => $crumb['position'],
                "name"      => $crumb['name'],
                "item"      => $crumb['item']
            ];
        }

        return sprintf('<script type="application/ld+json">%s</script>', json_encode($jsonLDBreadcrumb, JSON_UNESCAPED_SLASHES));
    }

    /**
     * Render in <head> meta/link/script
     * @param string $title
     * @param string $description
     * @param array $keywords
     * @param array $openGraph
     * @param array $jsonLd
     * @param array $breadcrumbItems
     * @return string
     */
    public function renderHead(string $title = "", string $description = "", array $keywords = [], array $openGraph = [], array $jsonLd = [], array $breadcrumbItems = []): string
    {
        return $this->renderTitle($title).
            $this->renderDescription($description).
            $this->renderKeywords($keywords).
            $this->renderCanonical().
            $this->renderHrefLang().
            $this->renderOpenGraph($openGraph).
            $this->renderJsonLd($jsonLd).
            $this->renderJsonBreadcrumb($breadcrumbItems);
    }

    /**
     * Render in <body> breadcrumb
     * @param array $breadcrumbItems
     * @return string
     */
    public function renderHtmlBreadcrumb(array $breadcrumbItems = []): string
    {
        $this->setBreadcrumbList($breadcrumbItems);

        $links = [];
        foreach($this->breadcrumbList as $i => $crumb) {
            if($i !== count($this->breadcrumbList)-1) {
                $links[] = "<span class='crumb p{$crumb['position']}'><a class='link' href='{$crumb['item']}'>{$crumb['name']}</a></span>";
            } else {
                $links[] = "<span class='crumb p{$crumb['position']}'>{$crumb['name']}</span>";
            }
        }

        return sprintf('<div id="breadcrumb">%s</div>', implode($this->defaultOptions['bc-separator'], $links));
    }
}