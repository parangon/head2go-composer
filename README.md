### parangon/head2go

* head2go library (04.2024)


### INSTALL
```
composer.json
```
```
{
    "require": {
            "parangon/head2go": "1.*"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/parangon/head2go-composer.git"
        }
    ]
}
```

###
### Playground
```
require_once "./vendor/autoload.php";
use Parangon\Head2go\Head2Go;


/** define options[] */
$options = [
    'pre-title'     => "My Website",
    'post-title'    => "",
    'tl-separator'  => " | ",
    'description'   => "Brand new website",
    'keywords'      => ["foo", "bar"],
    'hrefLang'      => ["fr", "en"],
    'bc-separator'  => '<span class="sep"> --> </span>',
    'bc-ucfirst'    => true,
    'ssl'           => true
];

/** 
 * define breadcrumbItems[]
 * Ex: ["name"] or ['name' => "name", 'route' => "/name"] or ['name' => ['fr' => "nom", 'en' => "name"], ...]
**/
$breadcrumbItems = [
    '/'    => "Welcome",
    '/fr'  => "Accueil",
    '/en'  => ['name' => "Home"],
    '/bar' => [
        'name' => ['fr' => "PMU", 'en' => "PUB"],
        'route' => "/foo"
    ]
];


/** init */
$head2go = new Head2Go($options, $breadcrumbItems);


/** somewhere in your html */
<head>
    <?= $head2go->renderHead("title", "description"); ?>
</head>
<body>
    <?= renderHtmlBreadcrumb(['/bar' => "bypass initial name"]); ?>
</body>
```

#### Output for http://www.mydomain.ext/fr/bar
```
<head>
    <title>My Website | title</title>
    <meta name="description" content="description">
    <meta name="keywords" content="foo, bar">
    <link rel="canonical" href="https://www.mydomain.ext/fr/bar" />
    <link rel="alternate" href="https://www.mydomain.ext/fr/bar" hreflang="x-default" />
    <link rel="alternate" href="https://www.mydomain.ext/en/bar" hreflang="en" />
    <script type="application/ld+json">
        {
            "@context":"https://schema.org",
            "@type":"BreadcrumbList",
            "itemListElement":[{
                "@type":"ListItem",
                "position":1,
                "name":"Accueil",
                "item":"https://www.mydomain.ext/fr"
            },{
                "@type":"ListItem",
                "position":2,
                "name":"PMU",
                "item":"https://www.mydomain.ext/fr/bar"
            }]
        }
    </script>
    <!-- here og:meta if not empty -->
    <!-- here json+ld:script if not empty -->
</head>
<body>
    <div id="breadcrumb">
        <span class="crumb p1"><a class="link" href="https://www.mydomain.ext/fr">Accueil</a></span>
        <span class="sep"> --> </span>
        <span class="crumb p2">Bypass initial name</span>
    </div>
</body>
```

###
### Methods available
```
renderHead($title, $description, $keywords[], $openGraph[], $jsonLd[], $breadcrumbUris[])
//-> renderTitle($title): pre-title + title + post-title
//-> renderDescription($description): description
//-> renderKeywords($keywords[]): keywords
//-> renderCanonical($url): canonical
//-> renderHrefLang($alternates[]): hreflang
//-> renderOpenGraph($openGraph[]): openGraph
//-> renderJsonLd($schema[]): json-ld
//-> renderJsonBreadcrumb($breadcrumbItems[]): json-ld breadcrumb

renderHtmlBreadcrumb($breadcrumbItems[]): html breadcrumb
```
<br>
contact : dev@parangon-creations.com